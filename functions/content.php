<?php 

//@param string $title - title
//@return string $slug -valid url slug
function slug(string $title){
//nahrad vsetko okrem pismen za pomlcku v stringu $title
$slug = preg_replace('/[^\w+]/', '-', $title);
$slug = strtolower(preg_replace('/(-+)/', '-', $slug));
return $slug;
}